% materia(X,Y,C) donde X es el codigo, Y es el nombre de la materia, C es la carrera

materia("1934","LOGICA MATEMATICA ELEMENTAL","Ambas").
materia("1946","INTRODUCCION AL ALGEBRA","Ambas").
materia("1978","CALCULO I","Ambas").
materia("3300","INTRODUCCION A LA ALGORITMICA Y PROGRAMACION","Ambas").

materia("1947","ALGEBRA","Licenciatura").
materia("3327","GEOMETRIA","Analista").
materia("1984","CALCULO II","Ambas").
materia("1948","PROGRAMACION AVANZADA","Ambas").
materia("1949","ORGANIZACION DEL PROCESADOR","Ambas").
materia("3325","ESTRUCTURA DE DATOS Y ALGORITMOS","Analista").
materia("3301","ALGORITMOS I","Licenciatura").
materia("1937","ESTADISTICA","Ambas").
materia("1976","INGLES","Ambas").

materia("3302","ALGORITMO II","Licenciatura").
materia("3326","DISEÑO DE ALGORITMOS","Analista").
materia("1959","BASE DE DATOS","Ambas").
materia("3303","ANALISIS Y DISEÑO DE SISTEMAS","Ambas").
materia("3304","INGENIERIA DE SOFTWARE","Ambas").
materia("1956","ANALISIS COMPARATIVO DE LENGUAJES","Ambas").
materia("3305","GEOMETRIA Y ALGEBRA LINEAL","Licenciatura").
materia("1998","PROYECTO","Analista").
materia("6235","ESTUDIO DE LA REALIDAD NACIONAL","Ambas").
materia("3335","BASE DE DATOS II","Licenciatura").

materia("1962","SIMULACION","Licenciatura").
materia("1961","AUTOMATAS Y LENGUAJES","Licenciatura").
materia("1969","SEMINARIO DE REDACCION INFORMATIVA","Licenciatura").
materia("3306","TALLER DE DISEÑO DE SOFTWARE","Licenciatura").
materia("1967","INTELIGENCIA ARTIFICIAL","Licenciatura").

materia("1965","SISTEMAS OPERATIVOS","Licenciatura").
materia("1960","METODOLOGIA DE LA INVESTIGACION","Licenciatura").
materia("3307","COMPUTABILIDAD Y COMPLEJIDAD","Licenciatura").
materia("1968","TELECOMUNICACIONES Y SISTEMAS ","Licenciatura").


% año(C,A,Q) donde C es el codigo, A es el año y Q el cuatrimestre de la materia.

ano("1934","1","Primero").
ano("1946","1","Segundo").
ano("1978","1","Anual").
ano("3300","1","Anual").

ano("1947","2","Primero").
ano("3327","2","Primero").
ano("1984","2","Primero").
ano("1948","2","Primero").
ano("1949","2","Segundo").
ano("3325","2","Segundo").
ano("3301","2","Segundo").
ano("1937","2","Segundo").
ano("1976","2","Anual").

ano("3302","3","Primero").
ano("3326","3","Primero").
ano("1959","3","Primero").
ano("3303","3","Primero").
ano("3304","3","Segundo").
ano("1956","3","Segundo").
ano("3305","3","Segundo").
ano("1998","3","Segundo").
ano("6235","3","Segundo").
ano("3335","3","Segundo").

ano("1962","4","Primero").
ano("1961","4","Primero").
ano("1969","4","Primero").
ano("3306","4","Segundo").
ano("1967","4","Segundo").

ano("1965","5","Primero").
ano("1960","5","Primero").
ano("3307","5","Primero").
ano("1968","5","Segundo").

% correlativa(X,Y,C) donde Y es necesaria para cursar Y, y ambos son los codigos de las materias
% ademas C es la condicion de regularidad de Y

correlativa("1946","1934","Regular").

correlativa("1947","1946","Regular").
correlativa("1984","1978","Regular").
correlativa("1948","1934","Aprobada").
correlativa("3327","1946","Aprobada").
correlativa("1948","3300","Regular").
correlativa("1949","3300","Regular").
correlativa("3325","1948","Regular").
correlativa("3325","3300","Aprobada").
correlativa("3301","1948","Regular").
correlativa("3301","3300","Aprobada").
correlativa("1937","1978","Regular").

correlativa("3302","3301","Regular").
correlativa("3326","3325","Regular").
correlativa("1959","1948","Regular").
correlativa("1959","1976","Regular").
correlativa("3303","3301","Regular").
correlativa("3304","3303","Regular").
correlativa("1956","1948","Regular").
correlativa("1956","1949","Regular").
correlativa("3305","1934","Aprobada").
correlativa("3305","1946","Aprobada").
correlativa("3305","1947","Regular").
correlativa("1998","1959","Regular").
correlativa("1998","3303","Regular").
correlativa("3335","1956","Regular").
correlativa("3335","1959","Aprobada").

correlativa("1962","1937","Regular").
correlativa("1962","3303","Regular").
correlativa("1961","1956","Regular").
correlativa("3306","1959","Aprobada").
correlativa("3306","3301","Aprobada").
correlativa("3306","3302","Regular").
correlativa("1967","3301","Regular").

correlativa("1965","1949","Aprobada").
correlativa("1965","1962","Regular").
correlativa("1960","1934","Aprobada").
correlativa("1960","3304","Regular").
correlativa("3307","1934","Aprobada").
correlativa("3307","3302","Aprobada").
correlativa("3307","3305","Regular").
correlativa("1968","1965","Regular").
correlativa("1968","3303","Aprobada").

% Para consultas

materia_ano(X,Y,Z) :- ano(X,Z,_),materia(X,Y,_).
materia_ano_carrera(X,Y,C,Z) :- materia(X,Y,W),ano(X,Z,_),(W=="Ambas";W==C).
correlatividad(X,N,Y,C) :- correlativa(X,Y,C),materia(X,N,_).
necesitan(X,N,Y,M) :- correlativa(Y,X,_),materia(X,N,_),materia(Y,M,_).
