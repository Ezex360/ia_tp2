TRABAJO PRACTICO 2 - INTELIGENCIA ARTIFICIAL
Autor: Giachero Ezequiel, 39737931.

Requerimientos: 
	Python 3 (y dependecias de aima-python), Minisat, SWI-Prolog.

Los ejercicios se encuentran realizados en 2 modulos,
	> graphcolor
	> nqueens

Para ejecutar estos modulos se utiliza el comando python
	python -m tp2.[modulo]

En el caso de las nqueens los parametros que toma son:
	python -m tp2.nqueens [tamaño del tablero] [opcional - minisat]
ejemplo: python -m tp2.nqueens 8 1 (ejecuta el nqueens con tablero 8 reias y utiliza minisat)
ejemplo: python -m tp2.nqueens 8 (ejecuta el nqueens con tablero 8 reias y utiliza dpll)

En el caso del coloreo de grafos los parametros son:
	python -m tp2.graphcolor [numero de iteraciones] [cantidad de nodos por grafo] [string con los colores]
ejemplo: python -m tp2.graphcolor 5 4 "RGB" (ejecuta graphcolor para 5 grafos de 4 nodos cada uno, y utilizando los colores "RGB")

El ejercicio de prolog se encuentra realizado dentro del archivo computacion.pl
y se ejecuta normalmente con "prolog -s computacion.pl".


