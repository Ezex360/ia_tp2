from .graphcolor import run,getDefaults
import sys

if __name__ == '__main__':
	args = len(sys.argv)
	N,colors,iters = getDefaults()
	if args > 1:
		iters = int(sys.argv[1])		
	if args > 2:
		N = int(sys.argv[2])
	if args > 3:
		colors = sys.argv[3].strip()
	#print(colors)
	run(N,colors,iters)