from tp2.utils.search import *
from tp2.utils.utils import *
from tp2.utils.logic import *
from tp2.utils.csp import *
from time import perf_counter
from tp2.utils.utils import timeout

def getDefaults():
    return 10,"RGB", 1

def GraphToMap(graph):
    g = graph.graph_dict
    dic = defaultdict(list)
    for key in g:
        for val in g[key]:
            dic["N"+str(key)].append("N"+str(val))
    return dic

def run_dpll(size=10,colors="RGB",iters=1):
    graph = GraphToMap(RandomGraph(list(range(4)),2))
    graph_problem = MapColoringSAT(colors, graph)
    model, assigns = dpll_satisfiable(graph_problem)
    print(graph)
    print("dpll: ",{var for var, val in model.items() if val})


def run(size=10,colors="RGB",iters=1):
    headers = ["case","algorithm","asigns","time","solution"]
    cases = [GraphToMap(RandomGraph(list(range(size)),1)) for i in range(iters)]
    table = comparation(cases,[backtracking_search,dpll_satisfiable],headers,colors)
    table_to_csv(table,str(size)+"_graph_coloring.csv")


def comparation(cases,searchers,headers,colors="RGB",time=60):
    def search(searcher,graph,colors,time):
        with timeout(time):
            try:
                asigns = 0
                start = perf_counter()
                if(s == backtracking_search):
                    graph_problem = MapColoringCSP(colors, graph)
                    model = backtracking_search(graph_problem)
                    assigns = graph_problem.nassigns
                elif(s == dpll_satisfiable):
                    graph_problem = MapColoringSAT(colors, graph)
                    model, assigns = dpll_satisfiable(graph_problem)
                    #print("dpll: ",{var for var, val in model.items() if val})
                else:
                    print("Error: Searcher not defined.")
                stop = perf_counter()
                time = stop - start
                if model:
                    return [str(assigns),str(time),"yes"]
                else:
                    return ["",str(time),"no"]
            except TimeoutError:
                return ["","out",""]

    table,n = [],0
    for c in cases:
        n+=1
        for s in searchers:
            print("Working in {0} case {1}  ".format(name(s),n), end="\r")
            data = search(s,c,colors,time)
            table += [[str(n)]+[name(s)]+ data]
    print_table(table,headers)
    return table
        


