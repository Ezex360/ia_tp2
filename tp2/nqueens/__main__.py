from .nqueens_sat import run
import sys

if __name__ == '__main__':
	args = len(sys.argv)
	N = 8
	minisat = 0;
	if args > 1:
		N = int(sys.argv[1])
	if args > 2:
		minisat = 1
	run(N,minisat) 