from math import ceil
from tp2.utils.logic import *


import sys, linecache
import subprocess

file = open("nqueens_sat.cnf", mode='w')

def run(N=8,minisat=0):
    #Codificar las n-queens como un problema de satisfacibilidad proposicional
    #Y colocarlo en un archivo .cnf
    createCNF(N)

    #Invocar a minisat para resolver la formula cnf de ser posible
    if(minisat==1):
        subprocess.call(['minisat', 'nqueens_sat.cnf', 'nqueens.sol'])
        if N>3:
            printAnswer(N)
    else:
    #Tomar las clausulas del archivo y colocarlas en una expresion para
    #ser resueltas mediante el algoritmo DPLL
        test = PropKB()
        one_sentence = None
        clauses = getClausesFromCNF()
        for sentence in clauses:
            test.tell(sentence)
            if one_sentence is None:
                one_sentence =  sentence
                continue
            one_sentence = one_sentence & sentence
        model, nassigns = dpll_satisfiable(one_sentence)
        if model:
            print("solucion (numero de casilla):\n ",{var for var, val in model.items() if val})
        else:
            print("No satisfacible")
        


#Lee el archivo .cnf y lo transforma en Expr utilizables por python
def getClausesFromCNF():
    data = linecache.getline("nqueens_sat.cnf",2)
    n = data.split()[-1]

    clauses = []
    for i in range (3,3+int(n)):
        line = linecache.getline("nqueens_sat.cnf",i)
        sentence = []
        for x in line.split():
            num = int(x)
            if num == 0:
                line_clause = sentence[0]
                for s in sentence[1:]:
                    line_clause = line_clause +" | "+s 
                clauses.append(expr(line_clause))
            if num > 0:
                sentence.append("S"+x)
            else:
                sentence.append("~S"+str(int(x)*-1))
    return clauses


#Crea un archivo cnf conteniendo el problema para luego ser resuelto
def createCNF(N):
    global file

    counter = (N * (N - 1) * (5 * N - 1)) / 3
    counter += N

    # Imprime el File header en el formato DIMACS CNF
    file.write("c {0:d}-Queen to SAT converter\n".format(N))
    file.write("p cnf {0:d} {1:d}\n".format((N * N), int(counter)))

    printRowClauses(N)
    printColClauses(N)
    printDiagClauses(N)

    file.close()


def printRowClauses(N):
    lim = N * N + 1
    global file

    for i in range(1, lim):
        file.write("{0:d} ".format(i))
        if i % N == 0:
            file.write("0\n")
            

    # Loop atraves de todos los casilleros del tablero
    for i in range(1, lim):
        row = ceil(i / N)  # Determinar el numero de fila basado en el numero de casillero

        # Loop para imprimir las clausulas
        for j in range(i, row * N + 1):
            if j == i:  # skips si row = column
                continue
            file.write("-{0:d} -{1:d} 0\n".format(i, j))


def printColClauses(N):
    lim = N * N + 1

    # Loop atraves de todos los casilleros del tablero
    for i in range(1, lim):

        # Loop para imprimir las clausulas
        for j in range(i, lim, N):
            if j == i:  
                continue
            file.write("-{0:d} -{1:d} 0\n".format(i, j))


def printDiagClauses(N):
    lim = N * N + 1

    # Loop atraves de todos los casilleros del tablero
    for i in range(1, lim):
        # obtener row y column
        row = ceil(i / N)
        col = i % N

        if col == 0: col = N

        # Loop para escribir las clausulas Diagonales Left to Right
        for j in range(i, min(((N - col + row) * N + 1), lim), N + 1):
            if j == i:  # skips if row = column
                continue
            file.write("-{0:d} -{1:d} 0\n".format(i, j))

    # Loop atraves de todos los casilleros del tablero
    for i in range(1, lim):
        # Loop para escribir las clausulas Diagonales Right to Left
        for j in range(i, lim, N - 1):
            if j == i:  # skips if row = column
                continue
            elif ceil((j - (N - 1)) / N) == ceil(j / N):
                break
            file.write("-{0:d} -{1:d} 0\n".format(i, j))

#A partir de la solucion de minisat crea un archivo de texto
#que contiene el tablero solucion al problema.
def printAnswer(n):
    line = linecache.getline("nqueens.sol",2)
    line = list(map(int ,line.split(' ')))
    for i in range(len(line)-1):
        if int(line[i]) < 0:
            line[i] = 0
        else:
            line[i] = 1
    def chunks(l, n):
        for i in range(0, len(l)-1, n):
            yield l[i:i + n]
    l = (list(chunks(line, n)))
    file3 = open("ans.txt", 'w')
    for i in range(len(l)):
        file3.write(str(l[i]))
        file3.write("\n")