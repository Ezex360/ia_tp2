from .search import *
from .utils import *
from .logic import *
from .csp import *
import sys

def uno():
	clauses = []
	clauses.append(expr('Mistico ==> Inmortal'))
	clauses.append(expr('~Mistico ==> Mamifero'))
	clauses.append(expr('Inmortal | Mamifero ==> TieneCuernos'))
	clauses.append(expr('Magico <== TieneCuernos'))
	unicorn_kb = PropKB()
	one_sentence = expr('Void | ~Void')
	for clause in clauses:
		unicorn_kb.tell(clause)
		one_sentence = one_sentence & clause
	#print(one_sentence)
	#print(unicorn_kb.clauses)
	print("Enumeracion de modelos ?")
	print("\tEs Mistico : "	,unicorn_kb.ask_if_true(expr('Mistico')),
		"\n\tTiene Cuernos : ",unicorn_kb.ask_if_true(expr('TieneCuernos')),
		"\n\tEs magico : "	,unicorn_kb.ask_if_true(expr('Magico')))
	print("Resolucion")
	print("\tEs Mistico : ",pl_resolution(unicorn_kb, expr('Mistico')),
		"\n\tTiene Cuernos : ",pl_resolution(unicorn_kb, expr('TieneCuernos')),
		"\n\tEs Magico : ",pl_resolution(unicorn_kb, expr('Magico')))
	print(dpll_satisfiable(one_sentence))

def dos_good():
	clauses = []
	clauses.append(expr('A <=> B'))
	clauses.append(expr('E ==> D'))
	clauses.append(expr('C & F ==> ~B'))
	clauses.append(expr('E ==> D'))
	clauses.append(expr('B ==> F'))
	clauses.append(expr('B ==> C'))
	one_sentence = None
	ej3 = PropKB()
	for sentence in clauses:
		ej3.tell(sentence)
		if one_sentence is None:
			one_sentence =  sentence
			continue
		one_sentence = one_sentence & sentence

	print("DPLL: ",dpll_satisfiable(one_sentence))
	print(pl_resolution(ej3,expr('~A & ~B')))

def dos():
	clauses = []
	clauses.append(expr('1 <=> 2'))
	clauses.append(expr('5 ==> 4'))
	clauses.append(expr('3 & 6 ==> ~2'))
	clauses.append(expr('5 ==> 4'))
	clauses.append(expr('2 ==> 6'))
	clauses.append(expr('2 ==> 3'))
	one_sentence = None
	ej3 = PropKB()
	for sentence in clauses:
		ej3.tell(sentence)
		if one_sentence is None:
			one_sentence =  sentence
			continue
		one_sentence = one_sentence & sentence

	#print("DPLL: ",dpll_satisfiable(one_sentence))
	#print(pl_resolution(ej3,expr('~1 & ~2')))

def count(lex, alpha):
    assert not variables(alpha)
    symbols = lex
    return count_all(alpha, symbols, {})

def count_all(alpha, symbols, model):
    if not symbols:
        result = pl_true(alpha, model)
        assert result in (True, False)
        return int(result)
    else:
        P, rest = symbols[0], symbols[1:]
        return (count_all(alpha, rest, extend(model, P, True)) +
                count_all(alpha, rest, extend(model, P, False)))

def tres():
	A,B,C,D = symbols('A,B,C,D')
	lexico = [A,B,C,D]
	s1 = B | C
	s2 = ~A & ~B & ~C & ~D
	s3 = (A |'==>'| B) & A & ~B & C & D
	print("Modelos para s1:",count(lexico,s1))
	print("Modelos para s2:",count(lexico,s2))
	print("Modelos para s3:",count(lexico,s3))

def cuatro_OLD():
	test_csp = MapColoringCSP(list('RGB'), """X: Y Z; Y: Z""")
	print(test_csp.to_cnf())
	solution = min_conflicts(test_csp)
	test_csp.display(solution)

def cuatro():
	#Backtracking
	test_sat = MapColoringCSP(list('RGB'), """X: Y Z; Y: X Z; Z: X Y""")
	model = backtracking_search(test_sat)
	print(model)
	#SAT
	test_sat = MapColoringSAT(list('RG'), """X: Y Z""")
	print(test_sat)
	model = dpll_satisfiable(test_sat)
	if model:
		print({var for var, val in model.items() if val})
	else:
		print("model not found")
	

def cinco():
	#No sirve esto
	eight_queens = NQueensCSP(8)
	solution = min_conflicts(eight_queens)
	eight_queens.display(solution)

def GraphToMap(graph):
	g = graph.graph_dict
	print(g)
	dic = defaultdict(list)
	for key in g:
		for val in g[key]:
			dic["N"+str(key)].append("N"+str(val))
	return dic
            

def test():
	graph = GraphToMap(RandomGraph(list(range(3)),1))
	print(graph)
	test_sat = MapColoringCSP(list('RG'), graph)
	model = backtracking_search(test_sat)
	print(model)
	test_sat = MapColoringSAT(list('RG'), graph)
	print(test_sat)
	model = dpll_satisfiable(test_sat)
	if model:
		print({var for var, val in model.items() if val})
	else:
		print("model not found")



ejers = [uno,dos,tres,cuatro,cinco,test]

def run(ejer = 1):
	if ejer > len(ejers):
		print("Ejercicio no encontrado")
	else:
		ejers[ejer-1]()

if __name__ == '__main__':
	args = len(sys.argv)
	num_ejer = 0
	if args > 1:
		num_ejer = int(sys.argv[1])
	run(num_ejer)