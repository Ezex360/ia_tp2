from .search import *
from .utils import *
from .logic import *
from .csp import *
import sys

def uno():
	print(unify(expr('P(x,y,z)'),expr('P(A,B,B)')))
	print(unify(expr('Q( G(x,x) , y)'),expr('Q( y , G(A,B ))')))
	print(unify(expr('Older(Father(y),y)'),expr('Older(Father(x),Jhon)')))
	print(unify(expr('Knows(Father(y),y)'),expr('Knows(x,x)')))

def dos_2():
	clauses = []
	clauses.append(expr('King(x) & Greedy(x) ==> Evil(x)'))
	clauses.append(expr('King(John)'))
	clauses.append(expr('Greedy(John)'))
	clauses.append(expr('Greedy(Richard)'))
	clauses.append(expr('Brother(Richard,John)'))
	evil_kb = FolKB(clauses)
	print(evil_kb.clauses)
	answer = fol_bc_ask(evil_kb, expr('Evil(x)'))
	print(list(answer))

def dos():
	clauses = []
	clauses.append(expr("(American(x) & Weapon(y) & Sells(x, y, z) & Hostile(z)) ==> Criminal(x)"))
	clauses.append(expr("Enemy(Nono, America)"))
	clauses.append(expr("Owns(Nono, M1)"))
	clauses.append(expr("Missile(M1)"))
	clauses.append(expr("(Missile(x) & Owns(Nono, x)) ==> Sells(West, x, Nono)"))
	clauses.append(expr("American(West)"))
	clauses.append(expr("Missile(x) ==> Weapon(x)"))
	clauses.append(expr("Enemy(x, America) ==> Hostile(x)"))
	crime_kb = FolKB(clauses)
	answer = fol_bc_ask(crime_kb, expr('Criminal(x)'))
	print(list(answer))


def tres():
	print("ndeahh","3")

def cuatro():
	print("ndeahh","4")	

def cinco():
	print("ndeahh","5")            

def test():
	print("ndeahh","test")



ejers = [uno,dos,tres,cuatro,cinco,test]

def run(ejer = 1):
	if ejer > len(ejers):
		print("Ejercicio no encontrado")
	else:
		ejers[ejer-1]()

if __name__ == '__main__':
	args = len(sys.argv)
	num_ejer = 0
	if args > 1:
		num_ejer = int(sys.argv[1])
	run(num_ejer)